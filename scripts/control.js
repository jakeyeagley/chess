var chessboard = [];

var whitesTurn = true;
var pieceSelected = false;

function initialBoard() {   
    pixel_height = 1;
    pixel_width = 1;
    
    for (var col = 0; col < 8; col++){
        
        for (var row = 0; row < 8; row++){     
            var cell = $('<div></div>');
            
            if (col % 2 == 0 && row % 2 == 0) {
                cell.css({position:'absolute',width:64,height:64,left:pixel_width,top:pixel_height,background:'green'});
            }
            else if (col % 2 == 1 && row % 2 == 1){
                cell.css({position:'absolute',width:64,height:64,left:pixel_width,top:pixel_height,background:'green'});
            }
            else{
                cell.css({position:'absolute',width:64,height:64,left:pixel_width,top:pixel_height,background:'white'});
            }
            $('#board').append(cell);
            pixel_width += 64;
        }   
        
        pixel_height += 64;
        pixel_width = 1;
    }
    
    var board = $('#board>div');
    $(board[0]).prepend('<img color="black" piece="rook" count=1 src="images/rookBlack.png" />').attr("align","center");
    $(board[1]).prepend('<img color="black" piece="knight" count=1 src="images/knightBlack.png" />').attr("align","center");
    $(board[2]).prepend('<img color="black" piece="bishop" count=1 src="images/bishopBlack.png" />').attr("align","center");
    $(board[3]).prepend('<img color="black" piece="king" count=1 src="images/kingBlack.png" />').attr("align","center");
    $(board[4]).prepend('<img color="black" piece="queen" count=1 src="images/queenBlack.png" />').attr("align","center");
    $(board[5]).prepend('<img color="black" piece="bishop" count=2 src="images/bishopBlack.png" />').attr("align","center");
    $(board[6]).prepend('<img color="black" piece="knight" count=2 src="images/knightBlack.png" />').attr("align","center");
    $(board[7]).prepend('<img color="black" piece="rook" count=2 first="true" src="images/rookBlack.png" />').attr("align","center");
    $(board[8]).prepend('<img color="black" piece="pawn" count=1 first="true" src="images/pawnBlack.png" />').attr("align","center");
    $(board[9]).prepend('<img color="black" piece="pawn" count=2 first="true" src="images/pawnBlack.png" />').attr("align","center");
    $(board[10]).prepend('<img color="black" piece="pawn" count=3 first="true" src="images/pawnBlack.png" />').attr("align","center");
    $(board[11]).prepend('<img color="black" piece="pawn" count=4 first="true" src="images/pawnBlack.png" />').attr("align","center");
    $(board[12]).prepend('<img color="black" piece="pawn" count=5 first="true" src="images/pawnBlack.png" />').attr("align","center");
    $(board[13]).prepend('<img color="black" piece="pawn" count=6 first="true" src="images/pawnBlack.png" />').attr("align","center");
    $(board[14]).prepend('<img color="black" piece="pawn" count=7 first="true" src="images/pawnBlack.png" />').attr("align","center");
    $(board[15]).prepend('<img color="black" piece="pawn" count=8 first="true" src="images/pawnBlack.png" />').attr("align","center");
    
    $(board[48]).prepend('<img color="white" piece="pawn" count=1 first="true" src="images/pawnWhite.png" />').attr("align","center");
    $(board[49]).prepend('<img color="white" piece="pawn" count=2 first="true" src="images/pawnWhite.png" />').attr("align","center");
    $(board[50]).prepend('<img color="white" piece="pawn" count=3 first="true" src="images/pawnWhite.png" />').attr("align","center");
    $(board[51]).prepend('<img color="white" piece="pawn" count=4 first="true" src="images/pawnWhite.png" />').attr("align","center");
    $(board[52]).prepend('<img color="white" piece="pawn" count=5 first="true" src="images/pawnWhite.png" />').attr("align","center");
    $(board[53]).prepend('<img color="white" piece="pawn" count=6 first="true" src="images/pawnWhite.png" />').attr("align","center");
    $(board[54]).prepend('<img color="white" piece="pawn" count=7 first="true" src="images/pawnWhite.png" />').attr("align","center");
    $(board[55]).prepend('<img color="white" piece="pawn" count=8 first="true" src="images/pawnWhite.png" />').attr("align","center");
    $(board[56]).prepend('<img color="white" piece="rook" count=1 src="images/rookWhite.png" />').attr("align","center");
    $(board[57]).prepend('<img color="white" piece="knight" count=1 src="images/knightWhite.png" />').attr("align","center");
    $(board[58]).prepend('<img color="white" piece="bishop" count=1 src="images/bishopWhite.png" />').attr("align","center");
    $(board[59]).prepend('<img color="white" piece="king" count=1 src="images/kingWhite.png" />').attr("align","center");
    $(board[60]).prepend('<img color="white" piece="queen" count=1 src="images/queenWhite.png" />').attr("align","center");
    $(board[61]).prepend('<img color="white" piece="bishop" count=2 src="images/bishopWhite.png" />').attr("align","center");
    $(board[62]).prepend('<img color="white" piece="knight" count=2 src="images/knightWhite.png" />').attr("align","center");
    $(board[63]).prepend('<img color="white" piece="rook" count=2 src="images/rookWhite.png" />').attr("align","center");
    
    //moving board, a single D array, into chessboard, a two D array
    var k = 0
    for (var i = 0; i < 8; i++){
        chessboard[i] = [];
        for (var j = 0; j < 8; j++){
//            console.log($(board[k]).html());
            chessboard[i][j] = $(board[k]);
            k++;
        }
    }
    
    
}

function makeMove() {
    var firstClick = true;
    
    var location = [];
    var color;
    var piece;
    var count;
    var square;
    
    $('#board').click(function(event) {
        if (firstClick) {
            color = $(event.target).attr('color');
            piece = $(event.target).attr('piece');
            count = $(event.target).attr('count');

            if(color != undefined && piece != undefined && count != undefined){   
                square = event.target.parentElement;
                $(square).css({width:56,height:56,border:'4px solid black'})

                location = getLocationOfPiece(color, piece, count, square);
                console.log('location: ', location);
                
                firstClick = false;
            }
        }
        else{
            if(piece == 'pawn'){
                movePawn(location, color, square);
            }
            if(piece == 'knight'){
                moveKnight(location, color, square);   
            }
            if(piece == 'rook'){
                moveRook(location, color, square);
            }
            if(piece == 'bishop'){
                moveBishop(location, color, square);
            }
            if(piece == 'queen'){
                moveQueen(location, color, square);
            }
            firstClick = true;
        }
    })
}
function getLocationOfPiece(color, piece, count, square){ 
    for (var i = 0; i < 8; i++){
        for (var j = 0; j < 8; j++){
            if($(chessboard[i][j]).get(0) == square){
                return [i,j];
            }
        }
    }
}
function getLocationOfMove(square, selectedColor){
    for (var i = 0; i < 8; i++){
        for (var j = 0; j < 8; j++){
            // if location moving to is a blank square or a square with an opposite colored piece
            if ($(chessboard[i][j]).get(0) == square || ($(chessboard[i][j])[0] == $(square).parent()[0] && selectedColor != $(square).attr('color'))){
                return [i,j];
            }
        }
    }
}
function moveRook(fromLocation, color, selectedSquare) {
    $(selectedSquare).css({width:64,height:64,border:''});
    
    toLocation = getLocationOfMove(event.target, color);
    
    console.log('toLocation: ', toLocation);
    console.log('fromLocation: ', fromLocation);
    console.log('event.target', $(event.target).attr('color'));
    
    if (toLocation == undefined){
        return;
    }
    
    if($(event.target).attr('color') != undefined){
        if($(event.target).attr('color') == color){
            return; //can not attack same colored piece
        }
        
    }
    
    to0 = toLocation[0];
    to1 = toLocation[1];
    from0 = fromLocation[0];
    from1 = fromLocation[1];   
    
    var piece_in_col_up = false;
    var piece_in_col_down = false;
    var piece_in_row_right = false;
    var piece_in_row_left = false;
    
    var position_col_up = [];
    var position_col_down = [];
    var position_row_right = [];
    var position_row_left = [];
    
    for(var index = 0; index < 8; index++){
        if(from0 - index >= 0){
            if($(chessboard[from0 - index][from1]).children()[0] != undefined  && !piece_in_col_up && $(chessboard[from0 - index][from1]).children().attr('piece') != 'rook'){
                piece_in_col_up = true;
                position_col_up[0] = from0 - index 
                position_col_up[1] = from1;
            }
        }
        if(from0 + index < 8){
            if($(chessboard[from0 + index][from1]).children()[0] != undefined  && !piece_in_col_down && $(chessboard[from0 + index][from1]).children().attr('piece') != 'rook'){
                piece_in_col_down = true;
                position_col_down = [from0 + index,from1];
            }
        }
        if(from1 - index >= 0){
            if($(chessboard[from0][from1 - index]).children()[0] != undefined  && !piece_in_row_left && $(chessboard[from0][from1 - index]).children().attr('piece') != 'rook'){
                piece_in_row_left = true;
                position_row_left = [from0,from1 - index];
            }
        }
        if(from1 + index < 8){
            if($(chessboard[from0][from1 + index]).children()[0] != undefined && !piece_in_row_right && $(chessboard[from0][from1 + index]).children().attr('piece') != 'rook'){
                piece_in_row_right = true;
                position_row_right = [from0,from1 + index];
            }
        }
    }
    for(var index = 0; index < 8; index++){
        if (from0 - index == to0 && from1 == to1 && (toLocation[0] >= position_col_up[0] || position_col_up.length == 0)){
            console.log(toLocation[0], position_col_up[0])
            $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
            $(chessboard[to0][to1]).attr("align","center");
            $(chessboard[to0][to1]).children().attr("first", "false");

            $(chessboard[from0][from1]).children().attr('img');
            whitesTurn = !whitesTurn;
        }
        if (from0 + index == to0 && from1 == to1 && (toLocation[0] <= position_col_down[0] || position_col_down.length == 0)){
            console.log(toLocation[0], position_col_down[0])
            $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
            $(chessboard[to0][to1]).attr("align","center");
            $(chessboard[to0][to1]).children().attr("first", "false");

            $(chessboard[from0][from1]).children().remove('img');
            whitesTurn = !whitesTurn;
        }
        if (from0 == to0 && from1 + index == to1 && (toLocation[1] <= position_row_right[1] || position_row_right.length == 0)){
            $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
            $(chessboard[to0][to1]).attr("align","center");
            $(chessboard[to0][to1]).children().attr("first", "false");

            $(chessboard[from0][from1]).children().remove('img');
            whitesTurn = !whitesTurn;
        }
        if (from0 == to0 && from1 - index == to1 && (toLocation[1] >= position_row_left[1] || position_row_left.length == 0)){
            $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
            $(chessboard[to0][to1]).attr("align","center");
            $(chessboard[to0][to1]).children().attr("first", "false");

            $(chessboard[from0][from1]).children().remove('img');
            whitesTurn = !whitesTurn;
        }
    }
}
function moveKnight(fromLocation, color, selectedSquare) {
    $(selectedSquare).css({width:64,height:64,border:''});
    
    toLocation = getLocationOfMove(event.target, color);

    console.log('toLocation: ', toLocation);
    console.log('fromLocation: ', fromLocation);
    
    if (toLocation == undefined){
        return;
    }
    
    to0 = toLocation[0];
    to1 = toLocation[1];
    from0 = fromLocation[0];
    from1 = fromLocation[1];
    
    if (from0 - 2 == to0 && from1 - 1 == to1){
        $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
        $(chessboard[to0][to1]).attr("align","center");
        $(chessboard[to0][to1]).children().attr("first", "false");

        $(chessboard[from0][from1]).children().remove('img');
        whitesTurn = !whitesTurn;
    }
    else if (from0 - 2 == to0 && from1 + 1 == to1){
        $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
        $(chessboard[to0][to1]).attr("align","center");
        $(chessboard[to0][to1]).children().attr("first", "false");

        $(chessboard[from0][from1]).children().remove('img');
        whitesTurn = !whitesTurn;
    }
    else if (from0 + 2 == to0 && from1 - 1 == to1){
        $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
        $(chessboard[to0][to1]).attr("align","center");
        $(chessboard[to0][to1]).children().attr("first", "false");

        $(chessboard[from0][from1]).children().remove('img');
        whitesTurn = !whitesTurn;
    }
    else if (from0 + 2 == to0 && from1 + 1 == to1){
        $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
        $(chessboard[to0][to1]).attr("align","center");
        $(chessboard[to0][to1]).children().attr("first", "false");

        $(chessboard[from0][from1]).children().remove('img');
        whitesTurn = !whitesTurn;
    }
    else if (from0 - 1 == to0 && from1 - 2 == to1){
        $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
        $(chessboard[to0][to1]).attr("align","center");
        $(chessboard[to0][to1]).children().attr("first", "false");

        $(chessboard[from0][from1]).children().remove('img');
        whitesTurn = !whitesTurn;
    }
    else if (from0 - 1 == to0 && from1 + 2 == to1){
        $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
        $(chessboard[to0][to1]).attr("align","center");
        $(chessboard[to0][to1]).children().attr("first", "false");

        $(chessboard[from0][from1]).children().remove('img');
        whitesTurn = !whitesTurn;
    }
    else if (from0 + 1 == to0 && from1 - 2 == to1){
        $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
        $(chessboard[to0][to1]).attr("align","center");
        $(chessboard[to0][to1]).children().attr("first", "false");

        $(chessboard[from0][from1]).children().remove('img');
        whitesTurn = !whitesTurn;
    }
    else if (from0 + 1 == to0 && from1 + 2 == to1){
        $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
        $(chessboard[to0][to1]).attr("align","center");
        $(chessboard[to0][to1]).children().attr("first", "false");

        $(chessboard[from0][from1]).children().remove('img');
        whitesTurn = !whitesTurn;
    }
    else {
        return;
    }
}
function movePawn(fromLocation, color, selectedSquare){
    $(selectedSquare).css({width:64,height:64,border:''});

    toLocation = getLocationOfMove(event.target, color);

    if(toLocation == undefined){
        return;
    }

    console.log('toLocation: ', toLocation);
    console.log('fromLocation: ', fromLocation);

    to0 = toLocation[0];
    to1 = toLocation[1];
    from0 = fromLocation[0];
    from1 = fromLocation[1];

    if (color == 'white'){
        if (from0 - 1 == to0 && from1 == to1){
            $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
            $(chessboard[to0][to1]).attr("align","center");
            $(chessboard[to0][to1]).children().attr("first", "false");

            $(chessboard[from0][from1]).children().remove('img');
            whitesTurn = !whitesTurn;
        }
        else if (from0 - 2 == to0 && from1 == to1 && $(chessboard[from0][from1]).children().attr("first") == "true") {
            $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
            $(chessboard[to0][to1]).attr("align","center");
            $(chessboard[to0][to1]).children().attr("first", "false");

            $(chessboard[from0][from1]).children().remove('img');
            whitesTurn = !whitesTurn;
        }
        else {}
    }
    if (color == 'black'){
        if (from0 + 1 == to0 && from1 == to1){
            $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
            $(chessboard[to0][to1]).attr("align","center");
            $(chessboard[to0][to1]).children().attr("first", "false");

//                console.log('img tag: ', $(chessboard[to0][to1]).children()[0]);
            $(chessboard[from0][from1]).children().remove('img');
            whitesTurn = !whitesTurn;
        }
        else if (from0 + 2 == to0 && from1 == to1 && $(chessboard[from0][from1]).children().attr("first") == "true") {
            $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
            $(chessboard[to0][to1]).attr("align","center");
            $(chessboard[to0][to1]).children().attr("first", "false");

//                console.log($(chessboard[to0][to1]).children()[0]);
            $(chessboard[from0][from1]).children().remove('img');
            whitesTurn = !whitesTurn;
        }
        else {}
    }
}
function moveBishop(fromLocation, color, selectedSquare){
     $(selectedSquare).css({width:64,height:64,border:''});
    
    toLocation = getLocationOfMove(event.target, color);
    
    console.log('toLocation: ', toLocation);
    console.log('fromLocation: ', fromLocation);
    console.log('event.target', $(event.target).attr('color'));
    
    if (toLocation == undefined){
        return;
    }
    
    if($(event.target).attr('color') != undefined){
        if($(event.target).attr('color') == color){
            return; //can not attack same colored piece
        }
        
    }
    
    to0 = toLocation[0];
    to1 = toLocation[1];
    from0 = fromLocation[0];
    from1 = fromLocation[1];   
    
    var piece_right_top = false;
    var piece_left_top = false;
    var piece_right_bottom = false;
    var piece_left_bottom = false;
    
    var position_right_top = [];
    var position_left_top = [];
    var position_right_bottom = [];
    var position_left_bottom = [];
    
    for(var index = 0; index < 8; index++){
        if(from0 - index >= 0 && from1 + index < 8){
            if($(chessboard[from0 - index][from1 + index]).children()[0] != undefined  && !piece_right_top && $(chessboard[from0 - index][from1 + index]).children().attr('piece') != 'bishop'){
                piece_right_top = true;
                position_right_top[0] = from0 - index 
                position_right_top[1] = from1 + index;
            }
        }
        if(from0 - index >= 0 && from1 - index >= 0){
            if($(chessboard[from0 - index][from1 - index]).children()[0] != undefined  && !piece_left_top && $(chessboard[from0 - index][from1 - index]).children().attr('piece') != 'bishop'){
                piece_left_top = true;
                position_left_top = [from0 - index,from1 - index];
            }
        }
        if(from0 + index < 8 && from1 + index < 8){
            if($(chessboard[from0 + index][from1 + index]).children()[0] != undefined  && !piece_right_bottom && $(chessboard[from0 + index][from1 + index]).children().attr('piece') != 'bishop'){
                piece_right_bottom = true;
                position_right_bottom = [from0 + index,from1 + index];
            }
        }
        if(from0 + index < 8 && from1 - index >= 0){
            if($(chessboard[from0 + index][from1 - index]).children()[0] != undefined && !piece_left_bottom && $(chessboard[from0 + index][from1 - index]).children().attr('piece') != 'bishop'){
                piece_left_bottom = true;
                position_left_bottom = [from0 + index,from1 - index];
            }
        }
    }
    for(var index = 0; index < 8; index++){
        if (from0 - index == to0 && from1 + index == to1 && ((to0 > position_right_top[0] && to1 < position_right_top[1]) || position_right_top.length == 0)){
            console.log(toLocation, position_right_top)
            $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
            $(chessboard[to0][to1]).attr("align","center");
            $(chessboard[to0][to1]).children().attr("first", "false");

            $(chessboard[from0][from1]).children().attr('img');
            whitesTurn = !whitesTurn;
        }
        if (from0 - index == to0 && from1 - index == to1 && ((to0 > position_left_top[0] && to1 > position_left_top[1]) || position_left_top.length == 0)){
            console.log(toLocation[0], position_left_top[0])
            $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
            $(chessboard[to0][to1]).attr("align","center");
            $(chessboard[to0][to1]).children().attr("first", "false");

            $(chessboard[from0][from1]).children().remove('img');
            whitesTurn = !whitesTurn;
        }
        if (from0 + index == to0 && from1 + index == to1 && ((to0 < position_right_bottom[0] && to1 < position_right_bottom[1]) || position_right_bottom.length == 0)){
            $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
            $(chessboard[to0][to1]).attr("align","center");
            $(chessboard[to0][to1]).children().attr("first", "false");

            $(chessboard[from0][from1]).children().remove('img');
            whitesTurn = !whitesTurn;
        }
        if (from0 + index == to0 && from1 - index == to1 && ((to0 < position_left_bottom[0] && to1 > position_left_bottom[1]) || position_left_bottom.length == 0)){
            $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
            $(chessboard[to0][to1]).attr("align","center");
            $(chessboard[to0][to1]).children().attr("first", "false");

            $(chessboard[from0][from1]).children().remove('img');
            whitesTurn = !whitesTurn;
        }
    }
}
function moveQueen(fromLocation, color, selectedSquare){
     $(selectedSquare).css({width:64,height:64,border:''});
    
    toLocation = getLocationOfMove(event.target, color);
    
    console.log('toLocation: ', toLocation);
    console.log('fromLocation: ', fromLocation);
    console.log('event.target', $(event.target).attr('color'));
    
    if (toLocation == undefined){
        return;
    }
    
    if($(event.target).attr('color') != undefined){
        if($(event.target).attr('color') == color){
            return; //can not attack same colored piece
        }
        
    }
    
    to0 = toLocation[0];
    to1 = toLocation[1];
    from0 = fromLocation[0];
    from1 = fromLocation[1];   
    
    var piece_in_col_up = false;
    var piece_in_col_down = false;
    var piece_in_row_right = false;
    var piece_in_row_left = false;
    
    var position_col_up = [];
    var position_col_down = [];
    var position_row_right = [];
    var position_row_left = [];
    
    var piece_right_top = false;
    var piece_left_top = false;
    var piece_right_bottom = false;
    var piece_left_bottom = false;
    
    var position_right_top = [];
    var position_left_top = [];
    var position_right_bottom = [];
    var position_left_bottom = [];
    
    for(var index = 0; index < 8; index++){
        if(from0 - index >= 0 && from1 + index < 8){
            if($(chessboard[from0 - index][from1 + index]).children()[0] != undefined  && !piece_right_top && $(chessboard[from0 - index][from1 + index]).children().attr('piece') != 'queen'){
                piece_right_top = true;
                position_right_top[0] = from0 - index 
                position_right_top[1] = from1 + index;
            }
        }
        if(from0 - index >= 0 && from1 - index >= 0){
            if($(chessboard[from0 - index][from1 - index]).children()[0] != undefined  && !piece_left_top && $(chessboard[from0 - index][from1 - index]).children().attr('piece') != 'queen'){
                piece_left_top = true;
                position_left_top = [from0 - index,from1 - index];
            }
        }
        if(from0 + index < 8 && from1 + index < 8){
            if($(chessboard[from0 + index][from1 + index]).children()[0] != undefined  && !piece_right_bottom && $(chessboard[from0 + index][from1 + index]).children().attr('piece') != 'queen'){
                piece_right_bottom = true;
                position_right_bottom = [from0 + index,from1 + index];
            }
        }
        if(from0 + index < 8 && from1 - index >= 0){
            if($(chessboard[from0 + index][from1 - index]).children()[0] != undefined && !piece_left_bottom && $(chessboard[from0 + index][from1 - index]).children().attr('piece') != 'queen'){
                piece_left_bottom = true;
                position_left_bottom = [from0 + index,from1 - index];
            }
        }
        if(from0 - index >= 0){
            if($(chessboard[from0 - index][from1]).children()[0] != undefined  && !piece_in_col_up && $(chessboard[from0 - index][from1]).children().attr('piece') != 'queen'){
                piece_in_col_up = true;
                position_col_up[0] = from0 - index 
                position_col_up[1] = from1;
            }
        }
        if(from0 + index < 8){
            if($(chessboard[from0 + index][from1]).children()[0] != undefined  && !piece_in_col_down && $(chessboard[from0 + index][from1]).children().attr('piece') != 'queen'){
                piece_in_col_down = true;
                position_col_down = [from0 + index,from1];
            }
        }
        if(from1 - index >= 0){
            if($(chessboard[from0][from1 - index]).children()[0] != undefined  && !piece_in_row_left && $(chessboard[from0][from1 - index]).children().attr('piece') != 'queen'){
                piece_in_row_left = true;
                position_row_left = [from0,from1 - index];
            }
        }
        if(from1 + index < 8){
            if($(chessboard[from0][from1 + index]).children()[0] != undefined && !piece_in_row_right && $(chessboard[from0][from1 + index]).children().attr('piece') != 'queen'){
                piece_in_row_right = true;
                position_row_right = [from0,from1 + index];
            }
        }
    }
    for(var index = 0; index < 8; index++){
        if (from0 - index == to0 && from1 + index == to1 && ((to0 > position_right_top[0] && to1 < position_right_top[1]) || position_right_top.length == 0)){
            console.log(toLocation, position_right_top)
            $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
            $(chessboard[to0][to1]).attr("align","center");
            $(chessboard[to0][to1]).children().attr("first", "false");

            $(chessboard[from0][from1]).children().attr('img');
            whitesTurn = !whitesTurn;
        }
        if (from0 - index == to0 && from1 - index == to1 && ((to0 > position_left_top[0] && to1 > position_left_top[1]) || position_left_top.length == 0)){
            console.log(toLocation[0], position_left_top[0])
            $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
            $(chessboard[to0][to1]).attr("align","center");
            $(chessboard[to0][to1]).children().attr("first", "false");

            $(chessboard[from0][from1]).children().remove('img');
            whitesTurn = !whitesTurn;
        }
        if (from0 + index == to0 && from1 + index == to1 && ((to0 < position_right_bottom[0] && to1 < position_right_bottom[1]) || position_right_bottom.length == 0)){
            $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
            $(chessboard[to0][to1]).attr("align","center");
            $(chessboard[to0][to1]).children().attr("first", "false");

            $(chessboard[from0][from1]).children().remove('img');
            whitesTurn = !whitesTurn;
        }
        if (from0 + index == to0 && from1 - index == to1 && ((to0 < position_left_bottom[0] && to1 > position_left_bottom[1]) || position_left_bottom.length == 0)){
            $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
            $(chessboard[to0][to1]).attr("align","center");
            $(chessboard[to0][to1]).children().attr("first", "false");

            $(chessboard[from0][from1]).children().remove('img');
            whitesTurn = !whitesTurn;
        }
        if (from0 - index == to0 && from1 == to1 && (toLocation[0] >= position_col_up[0] || position_col_up.length == 0)){
            console.log(toLocation[0], position_col_up[0])
            $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
            $(chessboard[to0][to1]).attr("align","center");
            $(chessboard[to0][to1]).children().attr("first", "false");

            $(chessboard[from0][from1]).children().attr('img');
            whitesTurn = !whitesTurn;
        }
        if (from0 + index == to0 && from1 == to1 && (toLocation[0] <= position_col_down[0] || position_col_down.length == 0)){
            console.log(toLocation[0], position_col_down[0])
            $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
            $(chessboard[to0][to1]).attr("align","center");
            $(chessboard[to0][to1]).children().attr("first", "false");

            $(chessboard[from0][from1]).children().remove('img');
            whitesTurn = !whitesTurn;
        }
        if (from0 == to0 && from1 + index == to1 && (toLocation[1] <= position_row_right[1] || position_row_right.length == 0)){
            $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
            $(chessboard[to0][to1]).attr("align","center");
            $(chessboard[to0][to1]).children().attr("first", "false");

            $(chessboard[from0][from1]).children().remove('img');
            whitesTurn = !whitesTurn;
        }
        if (from0 == to0 && from1 - index == to1 && (toLocation[1] >= position_row_left[1] || position_row_left.length == 0)){
            $(chessboard[to0][to1]).prepend($(chessboard[from0][from1]).children());
            $(chessboard[to0][to1]).attr("align","center");
            $(chessboard[to0][to1]).children().attr("first", "false");

            $(chessboard[from0][from1]).children().remove('img');
            whitesTurn = !whitesTurn;
        }
    }
}
function moveKing(fromLocation, color, selectedSquare){
    
}
$(function () { //on load
    initialBoard();
    makeMove();
});